// src/app.module.ts

import { Module } from '@nestjs/common';
import { NewsModule } from './news/news.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NewsSeeder } from './news/news.seeder';
import { News } from './news/entities/news.entity'; // Importa la entidad News

@Module({
  imports: [
    NewsModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT, 10),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      autoLoadEntities: true,
      synchronize: true,
    }),
    TypeOrmModule.forFeature([News]), // Agrega aquí la entidad News al array de features
  ],
  controllers: [],
  providers: [NewsSeeder],
})
export class AppModule {}
