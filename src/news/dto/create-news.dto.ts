import { IsString, IsNotEmpty, IsOptional, IsDate } from 'class-validator';

export class CreateNewsDto {
    @IsNotEmpty()
    @IsString()
    title: string;

    @IsOptional()
    @IsString()
    image?: string;

    @IsNotEmpty()
    publishedDate: Date;

    @IsNotEmpty()
    @IsString()
    place: string;

    @IsNotEmpty()
    @IsString()
    author: string;

    @IsNotEmpty()
    @IsString()
    content: string;
}
