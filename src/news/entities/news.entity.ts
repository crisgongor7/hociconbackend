import { Column, DeleteDateColumn, Entity } from "typeorm";

@Entity()
export class News {
    @Column({ primary: true, generated: true })
    id: number;

    @Column()
    title: string;

    @Column({ nullable: true, default: null })
    image?: string;

    @Column()
    publishedDate: Date;

    @Column()
    place: string;

    @Column()
    author: string;

    @Column()
    content: string;

    @DeleteDateColumn()
    deletedAt: Date;
}
