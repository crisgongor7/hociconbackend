// src/news/news.seeder.ts

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { News } from './entities/news.entity';

@Injectable()
export class NewsSeeder {
  constructor(
    @InjectRepository(News)
    private readonly newsRepository: Repository<News>,
  ) {}

  async seed() {
    await this.seedNews();
  }

  private async seedNews() {
    const newsData = [
      {
        title: 'Noticia 1',
        place: 'Lugar 1',
        author: 'Autor 1',
        content: 'Contenido de la noticia 1',
      },
      {
        title: 'Noticia 2',
        place: 'Lugar 2',
        author: 'Autor 2',
        content: 'Contenido de la noticia 2',
      },
    ];

    await Promise.all(newsData.map(async (data) => await this.newsRepository.save(data)));
  }
}
