import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { News } from './entities/news.entity';
import { Repository } from 'typeorm';

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News)
    private readonly newsRepository: Repository<News>,
  ) {}

  async create(createNewsDto: CreateNewsDto) {
    const news = this.newsRepository.create(createNewsDto);
    return await this.newsRepository.save(news);
  }

  async findAll() {
    return await this.newsRepository.find();
  }

  async findOne(id: number) {
    return await this.newsRepository.findOneBy({id});
  }

  async update(id: number, updateNewsDto: UpdateNewsDto) {
    await this.newsRepository.update(id, updateNewsDto);
    return `Cat with id ${id} updated`;
  }

  async remove(id: number) {
    await this.newsRepository.softDelete(id);
    return `Cat with id ${id} removed`;
  }
}
