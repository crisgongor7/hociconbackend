
INSTALACION
# Nombre del Proyecto

Descripción concisa del proyecto.

## Requisitos previos

Asegúrate de tener instaladas las siguientes herramientas antes de comenzar:

- [Node.js](https://nodejs.org/)
- [PostgreSQL](https://www.postgresql.org/) 16
- [Nest.js](https://nestjs.com/) (última versión)

## Configuración del entorno

1. **Restaurar la base de datos PostgreSQL:**
   - Utiliza una herramienta como `pg_restore` para restaurar la base de datos.
     ```bash
     pg_restore -U tu_usuario -d tu_base_de_datos tu_archivo_de_respaldo.dump
     ```

2. **Crear el archivo `.env`:**
   - Copia el archivo `.env.example` y renómbralo a `.env`.
   - Completa las variables de entorno con las credenciales de tu base de datos y otras configuraciones necesarias.

## Instalación

1. **Clona el repositorio:**
   ```bash
   https://gitlab.com/crisgongor7/hociconbackend.git
   Ingresa al directorio del proyecto:

bash
cd hocicon
Instala las dependencias con Yarn:

yarn install

y levanta el proyecto con yarn start:dev
